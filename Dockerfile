ARG CONTAINER_PROXY
FROM ${CONTAINER_PROXY}ubuntu:22.04

ARG CONTAINER_IMAGE
ENV CONTAINER_IMAGE="$CONTAINER_IMAGE"

RUN apt-get update && apt-get install -y --no-install-recommends \
    make=4.3-4.1build1 \
    sudo=1.9.9-1ubuntu2.4 \
    && rm -rf /var/lib/apt/lists/*
